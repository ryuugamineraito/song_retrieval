import wave
import os
import numpy as np
import sys
import yaml
sys.path.append('../')
from model.generate import build_fp,load_checkpoint,test_step

def load_config(config_fname):
    config_filepath = '../config/' + config_fname + '.yaml'
    if os.path.exists(config_filepath):
        print(f'cli: Configuration from {config_filepath}')
    else:
        sys.exit(f'cli: ERROR! Configuration file {config_filepath} is missing!!')

    with open(config_filepath, 'r') as f:
        cfg = yaml.safe_load(f)
    return cfg
def max_normalize(x):
    """
    Parameters
    ----------
    x : (float)

    Returns
    -------
    (float)
        Max-normalized audio signal.

    """
    if np.max(np.abs(x)) == 0:
        return x
    else:
        return x / np.max(np.abs(x))


def background_mix(x, x_bg, fs, snr_db):
    """
    Parameters
    ----------
    x : 1D array (float)
        Input audio signal.
    x_bg : 1D array (float)
        Background noise signal.
    fs : (float)
        Sampling rate.
    snr_db : (float)
        signal-to-noise ratio in decibel.

    Returns
    -------
    1D array
        Max-normalized mix of x and x_bg with SNR

    """
    # Check length
    if len(x) > len(x_bg):  # This will not happen though...
        _x_bg = np.zeros(len(x))
        bg_start = np.random.randint(len(x) - len(x_bg))
        bg_end = bg_start + len(x_bg)
        _x_bg[bg_start:bg_end] = x_bg
        x_bg = _x_bg
    elif len(x) < len(x_bg):  # This will not happen though...
        bg_start = np.random.randint(len(x_bg) - len(x))
        bg_end = bg_start + len(x)
        x_bg = x_bg[bg_start:bg_end]
    else:
        pass

    # Normalize with energy
    rmse_bg = np.sqrt(np.sum(x_bg**2 / len(x_bg)))
    x_bg = x_bg / rmse_bg
    rmse_x = np.sqrt(np.sum(x**2) / len(x))
    x = x / rmse_x

    # Mix
    magnitude = np.power(10, snr_db / 20.)
    x_mix = magnitude * x + x_bg
    return max_normalize(x_mix)
def load_audio(filename=str(),
               seg_start_sec=float(),
               offset_sec=0.0,
               seg_length_sec=float(),
               seg_pad_offset_sec=0.0,
               fs=22050,
               amp_mode='normal'):
    """
        Open file to get file info --> Calulate index range
        --> Load sample by index --> Padding --> Max-Normalize --> Out
        
    """
    start_frame_idx = np.floor(seg_start_sec).astype(int)
    seg_length_frame = np.floor(seg_length_sec * fs).astype(int)
    end_frame_idx = start_frame_idx + seg_length_frame

    # Get file-info
    file_ext = filename[-3:]
    # print(start_frame_idx, end_frame_idx)

    if file_ext == 'wav':
        pt_wav = wave.open(filename, 'r')
        pt_wav.setpos(start_frame_idx)
        x = pt_wav.readframes(end_frame_idx - start_frame_idx)
        x = np.frombuffer(x, dtype=np.int16)
        # print(x.shape)
        x = x / 2**15  # dtype=float
    else:
        raise NotImplementedError(file_ext)

    # Max Normalize, random amplitude
    if amp_mode == 'normal':
        pass
    elif amp_mode == 'max_normalize':
        _x_max = np.max(np.abs(x))
        if _x_max != 0:
            x = x / _x_max
    else:
        raise ValueError('amp_mode={}'.format(amp_mode))
    # print(x.shape)
    # padding process. it works only when win_size> audio_size and padding='random'
    audio_arr = np.zeros(int(seg_length_sec * fs))
    seg_pad_offset_idx = int(seg_pad_offset_sec * fs)
    #khuc1 nay set fs la 8000 roi ma ko hieu sao no lai len 16000 nen tui phai sua lai code lay 8000 dau thoi => con2 khuc nay nua ne 
    # if(len(x)<=fs): fs default la bao nhieu nhi 8000 y la de no chiu la phai 8000
    #     audio_arr[seg_pad_offset_idx:seg_pad_offset_idx + len(x)] = x
    # else:
    #     audio_arr[seg_pad_offset_idx:] = x[:fs] => tu them vao
    audio_arr[seg_pad_offset_idx:seg_pad_offset_idx + len(x)] = x # => code goc
    return audio_arr

def load_audio_multi_start(input,
                           seg_length_sec=float(),
                           fs=22050,
                           amp_mode='normal'):
    """ Load_audio wrapper for loading audio with multiple start indices. """
    # assert(len(seg_start_sec_list)==len(seg_length_sec))
    out = None
    for item in input:
    
        x = load_audio(filename=item[0],
                       seg_start_sec=item[1],
                       seg_length_sec=seg_length_sec,
                       fs=8000)
        x = x.reshape((1, -1))
        if out is None:
            out = x
        else:
            out = np.vstack((out, x))
    return out  # (B,T)
def get_fns_seg_list(fns_list=[],
                     segment_mode='all',
                     fs=22050,
                     duration=1,
                     hop=None):
    """
    return: fns_event_seg_list
        
        [[filename, seg_idx, offset_min, offset_max], [ ... ] , ... [ ... ]]
        
        offset_min is 0 or negative integer
        offset_max is 0 or positive integer
        
    """
    if hop == None: hop = duration
    fns_event_seg_list = []


    for offset_idx, filename in enumerate(fns_list):
        # Get audio info
        n_frames_in_seg = fs * duration
        n_frames_in_hop = fs * hop  # 2019 09.05
        file_ext = filename[-3:]

        if file_ext == 'wav':
            pt_wav = wave.open(filename, 'r')
            _fs = pt_wav.getframerate()

            if fs != _fs:
                raise ValueError('Sample rate should be {} but got {}'.format(
                    str(fs), str(_fs)))

            n_frames = pt_wav.getnframes()
            #n_segs = n_frames // n_frames_in_seg
            if n_frames > n_frames_in_seg:
                n_segs = (n_frames - n_frames_in_seg +
                          n_frames_in_hop) // n_frames_in_hop
            else:
                n_segs = 1

            n_segs = int(n_segs)
            assert (n_segs > 0)
            residual_frames = np.max([
                0,
                n_frames - ((n_segs - 1) * n_frames_in_hop + n_frames_in_seg)
            ])
            pt_wav.close()
        else:
            raise NotImplementedError(file_ext)

        # pdb.set_trace()
        # 'all', 'random_oneshot', 'first'
        if segment_mode == 'all':
            for seg_idx in range(n_segs):
                offset_min, offset_max = int(-1 *
                                             n_frames_in_hop), n_frames_in_hop
                frame = (seg_idx+1)*n_frames_in_hop
                start = frame + offset_min
                end = frame+offset_max
                if seg_idx == 0:  # first seg
                    offset_min = 0
                    start=0
                if seg_idx == (n_segs - 1):  # last seg
                    offset_max = residual_frames
                    
                
                
                fns_event_seg_list.append(
                    [filename, seg_idx, start, end])

                

        elif segment_mode == 'random_oneshot':
            seg_idx = np.random.randint(0, n_segs)
            offset_min, offset_max = n_frames_in_hop, n_frames_in_hop
            if seg_idx == 0:  # first seg
                offset_min = 0
            if seg_idx == (n_segs - 1):  # last seg
                offset_max = residual_frames
            fns_event_seg_list.append(
                [filename, seg_idx, offset_min, offset_max])
        elif segment_mode == 'first':
            seg_idx = 0
            offset_min, offset_max = 0, 0
            fns_event_seg_list.append(
                [filename, seg_idx, offset_min, offset_max])
        else:
            raise NotImplementedError(segment_mode)

        

    return fns_event_seg_list
cfg = load_config('default')
m_pre, m_fp = build_fp(cfg)
checkpoint_root_dir = '../logs/' + 'checkpoint/'
checkpoint_name = 'final_ckpt_logs'
checkpoint_index = '101'
checkpoint_index = load_checkpoint(checkpoint_root_dir, checkpoint_name,
                                       checkpoint_index, m_fp)
              
path = '../../wav-data/test_fs8k/subset_song/1073742443.wav'
a = get_fns_seg_list([path],fs=8000,duration=1,hop=0.5)
a = load_audio_multi_start(a,seg_length_sec=1.0,fs=8000)
a = np.expand_dims(a ,1).astype(np.float32)
# print(a.shape)

cfg = load_config('default')
emb = test_step(a, m_pre, m_fp)
print(emb.shape)