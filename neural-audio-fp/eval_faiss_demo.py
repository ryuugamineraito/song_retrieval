# -*- coding: utf-8 -*-
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
""" eval_faiss.py """
import os
import sys
import time
import glob
import click
import curses
import numpy as np
import pdb
import sys
import yaml
sys.path.append('./eval')
from eval.utils.get_index_faiss import get_index
from eval.utils.print_table import PrintTable
import wave
from model.generate import build_fp,load_checkpoint,test_step
def max_normalize(x):
    """
    Parameters
    ----------
    x : (float)

    Returns
    -------
    (float)
        Max-normalized audio signal.

    """
    if np.max(np.abs(x)) == 0:
        return x
    else:
        return x / np.max(np.abs(x))


def background_mix(x, x_bg, fs, snr_db):
    """
    Parameters
    ----------
    x : 1D array (float)
        Input audio signal.
    x_bg : 1D array (float)
        Background noise signal.
    fs : (float)
        Sampling rate.
    snr_db : (float)
        signal-to-noise ratio in decibel.

    Returns
    -------
    1D array
        Max-normalized mix of x and x_bg with SNR

    """
    # Check length
    if len(x) > len(x_bg):  # This will not happen though...
        _x_bg = np.zeros(len(x))
        bg_start = np.random.randint(len(x) - len(x_bg))
        bg_end = bg_start + len(x_bg)
        _x_bg[bg_start:bg_end] = x_bg
        x_bg = _x_bg
    elif len(x) < len(x_bg):  # This will not happen though...
        bg_start = np.random.randint(len(x_bg) - len(x))
        bg_end = bg_start + len(x)
        x_bg = x_bg[bg_start:bg_end]
    else:
        pass

    # Normalize with energy
    rmse_bg = np.sqrt(np.sum(x_bg**2 / len(x_bg)))
    x_bg = x_bg / rmse_bg
    rmse_x = np.sqrt(np.sum(x**2) / len(x))
    x = x / rmse_x

    # Mix
    magnitude = np.power(10, snr_db / 20.)
    x_mix = magnitude * x + x_bg
    return max_normalize(x_mix)
def load_audio(filename=str(),
               seg_start_sec=float(),
               offset_sec=0.0,
               seg_length_sec=float(),
               seg_pad_offset_sec=0.0,
               fs=22050,
               amp_mode='normal'):
    """
        Open file to get file info --> Calulate index range
        --> Load sample by index --> Padding --> Max-Normalize --> Out
        
    """
    start_frame_idx = np.floor(seg_start_sec).astype(int)
    seg_length_frame = np.floor(seg_length_sec * fs).astype(int)
    end_frame_idx = start_frame_idx + seg_length_frame

    # Get file-info
    file_ext = filename[-3:]
    # print(start_frame_idx, end_frame_idx)

    if file_ext == 'wav':
        pt_wav = wave.open(filename, 'r')
        pt_wav.setpos(start_frame_idx)
        x = pt_wav.readframes(end_frame_idx - start_frame_idx)
        x = np.frombuffer(x, dtype=np.int16)
        # print(x.shape)
        x = x / 2**15  # dtype=float
    else:
        raise NotImplementedError(file_ext)

    # Max Normalize, random amplitude
    if amp_mode == 'normal':
        pass
    elif amp_mode == 'max_normalize':
        _x_max = np.max(np.abs(x))
        if _x_max != 0:
            x = x / _x_max
    else:
        raise ValueError('amp_mode={}'.format(amp_mode))
    # print(x.shape)
    # padding process. it works only when win_size> audio_size and padding='random'
    audio_arr = np.zeros(int(seg_length_sec * fs))
    seg_pad_offset_idx = int(seg_pad_offset_sec * fs)
    #khuc1 nay set fs la 8000 roi ma ko hieu sao no lai len 16000 nen tui phai sua lai code lay 8000 dau thoi => con2 khuc nay nua ne 
    # if(len(x)<=fs): fs default la bao nhieu nhi 8000 y la de no chiu la phai 8000
    #     audio_arr[seg_pad_offset_idx:seg_pad_offset_idx + len(x)] = x
    # else:
    #     audio_arr[seg_pad_offset_idx:] = x[:fs] => tu them vao
    audio_arr[seg_pad_offset_idx:seg_pad_offset_idx + len(x)] = x # => code goc
    return audio_arr

def load_audio_multi_start(input,
                           seg_length_sec=float(),
                           fs=22050,
                           amp_mode='normal'):
    """ Load_audio wrapper for loading audio with multiple start indices. """
    # assert(len(seg_start_sec_list)==len(seg_length_sec))
    out = None
    for item in input:
    
        x = load_audio(filename=item[0],
                       seg_start_sec=item[1],
                       seg_length_sec=seg_length_sec,
                       fs=8000)
        x = x.reshape((1, -1))
        if out is None:
            out = x
        else:
            out = np.vstack((out, x))
    return out  # (B,T)
def get_fns_seg_list(fns_list=[],
                     segment_mode='all',
                     fs=22050,
                     duration=1,
                     hop=None):
    """
    return: fns_event_seg_list
        
        [[filename, seg_idx, offset_min, offset_max], [ ... ] , ... [ ... ]]
        
        offset_min is 0 or negative integer
        offset_max is 0 or positive integer
        
    """
    if hop == None: hop = duration
    fns_event_seg_list = []


    for offset_idx, filename in enumerate(fns_list):
        # Get audio info
        n_frames_in_seg = fs * duration
        n_frames_in_hop = fs * hop  # 2019 09.05
        file_ext = filename[-3:]

        if file_ext == 'wav':
            pt_wav = wave.open(filename, 'r')
            _fs = pt_wav.getframerate()

            if fs != _fs:
                raise ValueError('Sample rate should be {} but got {}'.format(
                    str(fs), str(_fs)))

            n_frames = pt_wav.getnframes()
            #n_segs = n_frames // n_frames_in_seg
            if n_frames > n_frames_in_seg:
                n_segs = (n_frames - n_frames_in_seg +
                          n_frames_in_hop) // n_frames_in_hop
            else:
                n_segs = 1

            n_segs = int(n_segs)
            assert (n_segs > 0)
            residual_frames = np.max([
                0,
                n_frames - ((n_segs - 1) * n_frames_in_hop + n_frames_in_seg)
            ])
            pt_wav.close()
        else:
            raise NotImplementedError(file_ext)

        # pdb.set_trace()
        # 'all', 'random_oneshot', 'first'
        if segment_mode == 'all':
            for seg_idx in range(n_segs):
                offset_min, offset_max = int(-1 *
                                             n_frames_in_hop), n_frames_in_hop
                frame = (seg_idx+1)*n_frames_in_hop
                start = frame + offset_min
                end = frame+offset_max
                if seg_idx == 0:  # first seg
                    offset_min = 0
                    start=0
                if seg_idx == (n_segs - 1):  # last seg
                    offset_max = residual_frames
                    
                
                
                fns_event_seg_list.append(
                    [filename, seg_idx, start, end])

                

        elif segment_mode == 'random_oneshot':
            seg_idx = np.random.randint(0, n_segs)
            offset_min, offset_max = n_frames_in_hop, n_frames_in_hop
            if seg_idx == 0:  # first seg
                offset_min = 0
            if seg_idx == (n_segs - 1):  # last seg
                offset_max = residual_frames
            fns_event_seg_list.append(
                [filename, seg_idx, offset_min, offset_max])
        elif segment_mode == 'first':
            seg_idx = 0
            offset_min, offset_max = 0, 0
            fns_event_seg_list.append(
                [filename, seg_idx, offset_min, offset_max])
        else:
            raise NotImplementedError(segment_mode)

        

    return fns_event_seg_list


def load_config(config_fname):
    config_filepath = './config/' + config_fname + '.yaml'
    if os.path.exists(config_filepath):
        print(f'cli: Configuration from {config_filepath}')
    else:
        sys.exit(f'cli: ERROR! Configuration file {config_filepath} is missing!!')

    with open(config_filepath, 'r') as f:
        cfg = yaml.safe_load(f)
    return cfg

def load_memmap_data(source_dir,
                     fname,
                     append_extra_length=None,
                     shape_only=False,
                     display=True):
    """
    Load data and datashape from the file path.

    • Get shape from [source_dir/fname_shape.npy}.
    • Load memmap data from [source_dir/fname.mm].

    Parameters
    ----------
    source_dir : (str)
    fname : (str)
        File name except extension.
    append_empty_length : None or (int)
        Length to appened empty vector when loading memmap. If activate, the
        file will be opened as 'r+' mode.
    shape_only : (bool), optional
        Return only shape. The default is False.
    display : (bool), optional
        The default is True.

    Returns
    -------
    (data, data_shape)

    """
    path_shape = source_dir + fname + '_shape.npy'
    path_data = source_dir + fname + '.mm'
    data_shape = np.load(path_shape)
    if shape_only:
        return data_shape

    if append_extra_length:
        data_shape[0] += append_extra_length
        data = np.memmap(path_data, dtype='float32', mode='r+',
                         shape=(data_shape[0], data_shape[1]))
    else:
        data = np.memmap(path_data, dtype='float32', mode='r',
                         shape=(data_shape[0], data_shape[1]))
    if display:
        print(f'Load {data_shape[0]:,} items from \033[32m{path_data}\033[0m.')
    return data, data_shape


# @click.command()
# @click.argument('emb_dir', required=True,type=click.STRING)
# @click.option('--emb_dummy_dir', default=None, type=click.STRING,
#               help="Specify a directory containing 'dummy_db.mm' and " +
#               "'dummy_db_shape.npy' to use. Default is EMB_DIR.")
# @click.option('--index_type', '-i', default='ivfpq', type=click.STRING,
#               help="Index type must be one of {'L2', 'IVF', 'IVFPQ', " +
#               "'IVFPQ-RR', 'IVFPQ-ONDISK', HNSW'}")
# @click.option('--nogpu', default=False, is_flag=True,
#               help='Use this flag to use CPU only.')
# @click.option('--max_train', default=1e7, type=click.INT,
#               help='Max number of items for index training. Default is 1e7.')
# @click.option('--test_seq_len', default='1 3 5 9 11 19', type=click.STRING,
#               help="A set of different number of segments to test. " +
#               "Numbers are separated by spaces. Default is '1 3 5 9 11 19'," +
#               " which corresponds to '1s, 2s, 3s, 5s, 6s, 10s'.")
# @click.option('--test_ids', '-t', default='icassp', type=click.STRING,
#               help="One of {'all', 'icassp', 'path/file.npy', (int)}. If 'all', " +
#               "test all IDs from the test. If 'icassp', use the 2,000 " +
#               "sequence starting point IDs of 'eval/test_ids_icassp.npy' " +
#               "located in ./eval directory. You can also specify the 1-D array "
#               "file's location. Any numeric input N (int) > 0 will randomly "
#               "select N IDs. Default is 'icassp'.")
# @click.option('--k_probe', '-k', default=20, type=click.INT,
#               help="Top k search for each segment. Default is 20")
# @click.option('--display_interval', '-dp', default=10, type=click.INT,
#               help="Display interval. Default is 10, which updates the table" +
#               " every 10 queries.")
def eval_faiss(emb_dir,
               emb_dummy_dir=None,
               index_type='ivfpq',
               nogpu=False,
               max_train=1e7,
               test_ids='icassp',
               test_seq_len='1 3 5 9 11 19',
               k_probe=20,
               display_interval=5):
    """
    Segment/sequence-wise audio search experiment and evaluation: implementation based on FAISS.

    ex) python eval.py EMB_DIR --index_type ivfpq

    EMB_DIR: Directory where {query, db, dummy_db}.mm files are located. The 'raw_score.npy' and 'test_ids.npy' will be also created in the same directory.



    """
    # test_seq_len = np.asarray(
        # list(map(int, test_seq_len.split())))  # '1 3 5' --> [1, 3, 5]

    # Load items from {query, db, dummy_db}
    # query, query_shape = load_memmap_data(emb_dir, 'query')
    db, db_shape = load_memmap_data(emb_dir, 'db')
    # if emb_dummy_dir is None:
    #     emb_dummy_dir = emb_dir
    # dummy_db, dummy_db_shape = load_memmap_data(emb_dummy_dir, 'dummy_db')
    """ ----------------------------------------------------------------------
    FAISS index setup

        dummy: 10 items.
        db: 5 items.
        query: 5 items, corresponding to 'db'.

        index.add(dummy_db); index.add(db) # 'dummy_db' first

               |------ dummy_db ------|
        index: [d0, d1, d2,..., d8, d9, d11, d12, d13, d14, d15]
                                       |--------- db ----------|

                                       |--------query ---------|
                                       [q0,  q1,  q2,  q3,  q4]

    • The set of ground truth IDs for q[i] will be (i + len(dummy_db))

    ---------------------------------------------------------------------- """
    # Create and train FAISS index
    # index = get_index(index_type, dummy_db, dummy_db.shape, (not nogpu),
    #                   max_train)
    index = get_index(index_type, db, db.shape, (not nogpu),
                      max_train)


    # Add items to index
    start_time = time.time()

    # index.add(dummy_db); print(f'{len(dummy_db)} items from dummy DB')
    index.add(db); print(f'{len(db)} items from reference DB')

    t = time.time() - start_time
    print(f'Added total {index.ntotal} items to DB. {t:>4.2f} sec.')

    """ ----------------------------------------------------------------------
    We need to prepare a merged {dummy_db + db} memmap:

    • Calcuation of sequence-level matching score requires reconstruction of
      vectors from FAISS index.
    • Unforunately, current faiss.index.reconstruct_n(id_start, id_stop)
      supports only CPU index.
    • We prepare a fake_recon_index thourgh the on-disk method.

    ---------------------------------------------------------------------- """
    # Prepare fake_recon_index
    # del dummy_db
    start_time = time.time()

    # if test_ids == "icassp":
    #     fake_recon_index, index_shape = load_memmap_data(
    #         emb_dummy_dir, 'dummy_db', append_extra_length=query_shape[0],
    #         display=False)
    #     fake_recon_index[dummy_db_shape[0]:dummy_db_shape[0] + query_shape[0], :] = db[:, :]
    #     fake_recon_index.flush()
    # elif test_ids == "zalo_ids.npy":
    #     fake_recon_index, index_shape = load_memmap_data(
    #         emb_dummy_dir, 'dummy_db', append_extra_length=db_shape[0],
    #         display=False)
    #     fake_recon_index[dummy_db_shape[0]:dummy_db_shape[0] + db_shape[0], :] = db[:, :]
    #     fake_recon_index.flush()
    fake_recon_index, index_shape = db, db_shape
    
    fake_recon_index.flush()
    t = time.time() - start_time
    print(f'Created fake_recon_index, total {index_shape[0]} items. {t:>4.2f} sec.')

    
    print(f'Load labels\n', end='')
    db_filename = emb_dir+ 'db_label.npy'
    db_label = np.load(db_filename)
    # query_filename = emb_dir+ 'query_label.npy'
    # query_label = np.load(query_filename)
    # _, query_index = np.unique(query_label, return_index=True)
    print(f'Finished\n',end='')
    # load model and generate query
    cfg = load_config('default')
    m_pre, m_fp = build_fp(cfg)
    checkpoint_root_dir = cfg['DIR']['LOG_ROOT_DIR'] + 'checkpoint/'
    checkpoint_name = 'final_ckpt_logs'
    checkpoint_index = '101'
    checkpoint_index = load_checkpoint(checkpoint_root_dir, checkpoint_name,
                                       checkpoint_index, m_fp)
    
    start_time = time.time()
    query_file = '../wav-data/test_fs8k/subset_song/1073742443.wav'
    list_start_seg = get_fns_seg_list([query_file],fs=8000,duration=1,hop=0.5)
    input_q = load_audio_multi_start(list_start_seg,seg_length_sec=1.0,fs=8000)
    input_q = np.expand_dims(input_q ,1).astype(np.float32)
    emb = test_step(input_q, m_pre, m_fp)
    q = emb.numpy()   

            # segment-level top k search for each segment
    _, I = index.search(
            q, k_probe) # _: distance, I: result IDs matrix

            # offset compensation to get the start IDs of candidate sequences
    for offset in range(len(I)):
        I[offset, :] -= offset

            # unique candidates
    candidates = np.unique(I[np.where(I >= 0)])   # ignore id < 0

      
    _scores = np.zeros(len(candidates))
    for ci, cid in enumerate(candidates):
        _scores[ci] = np.mean(
                np.diag(
                        # np.dot(q, index.reconstruct_n(cid, (cid + l)).T)
                    np.dot(q, fake_recon_index[cid:cid + q.shape[0], :].T)
                    )
                )

      
            # pdb.set_trace()
    pred_ids = candidates[np.argsort(-_scores)[:10]]
    pred_labels = db_label[pred_ids]
    print(pred_labels)

if __name__ == "__main__":
    emb_dir='./zalo_public/final_ckpt_logs/101/'
    eval_faiss(emb_dir=emb_dir)
