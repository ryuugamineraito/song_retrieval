from pathlib import Path 
import os
from pydub import AudioSegment
import pdb
import random
from tqdm import tqdm
import numpy as np

start = 0

if __name__ == "__main__":
    full_song_path = "/mnt/d/Deep_learning/hum_to_song/wav-data/test_fs8k/full_song"
    subset_song_path = "/mnt/d/Deep_learning/hum_to_song/wav-data/test_fs8k/subset_song"
    Path(subset_song_path).mkdir(parents = True, exist_ok = True)

    selected_songs = np.random.permutation(os.listdir(full_song_path))[:300]
    print(len(np.unique(selected_songs)))

    # Find min audio duration to trim
    audio_lengths = []

    for song in selected_songs:
        song_name = song.replace(".wav", "")
        audSeg = AudioSegment.from_wav(os.path.join(full_song_path, song))
        audSeg = audSeg.set_frame_rate(8000)
        audSeg = audSeg.set_channels(1)
        audLength = audSeg.duration_seconds
        audio_lengths.append(audLength)

    # pdb.set_trace()

    count = 0
    for song in tqdm(selected_songs):
        audSeg = AudioSegment.from_wav(os.path.join(full_song_path, song))
        audSeg = audSeg.set_frame_rate(8000)
        audSeg = audSeg.set_channels(1)

        start = random.randint(0, int(min(audio_lengths))-5)
        end = start + 5
        audSeg = audSeg[start*1000:end*1000]
        count += 1
        audSeg.export(os.path.join(subset_song_path, song), format="wav")

    print(count)