Generate zalo fingerprint
python run.py generate final_ckpt_logs 101 --source ZALO_SUBSET_SONG_SOURCE_ROOT_DIR --output FP_OUTPUT_DIR --skip_dummy
In FP_OUTPUT_DIR:
Change custom_db.mm to db.mm
Change custom_db_label.npy to db_label.npy
Change custom_db_shape.npy to db_shape.npy

Use file demo.ipynb to try searching
Change emb_dir to your FP_OUTPUT_DIR 
Change query_file to your query wav song dir
Change db_path to your zalo subset song dir
