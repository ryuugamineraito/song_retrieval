from pathlib import Path 
import os
from pydub import AudioSegment
import pdb
from tqdm import tqdm
start = 1
end = 6
if __name__ == "__main__":
    dest_dir = './query/'
    base_path = '.'

    f = '6426101.wav'
    # audSeg = AudioSegment.from_mp3(os.path.join(base_path, f))
    audSeg = AudioSegment.from_mp3(os.path.join(base_path, f))            
                # audSeg = AudioSegment.from_wav(os.path.join(base_path, folder, f))
    audSeg = audSeg.set_frame_rate(8000)
    audSeg = audSeg.set_channels(1)
    audSeg = audSeg[start*1000:end*1000]
    dest= os.path.join(dest_dir)
    if not os.path.isdir(dest):
        os.makedirs(dest)
    audSeg.export(os.path.join(dest,f), format="wav")
  

